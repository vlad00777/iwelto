function initMap() {

    var contentString = '<b style="font-size:14px;">P.P.H.U. Iwelto Premium Tomasz Grzywacz</b><br><br><p class="mb0">Bogucin 116, 21-080 Garbów</p><p class="mb0">tel: <a href="tel:695-165-940">695-165-940</a> Tomasz Grzywacz</p><p class="mb0">tel: <a href="tel:691-695-282">691-695-282</a> Izabela Grzywacz</p><p class="mb0">e-mail: <a href="mailto:tomasz.iwelto@interia.pl">tomasz.iwelto@interia.pl</a></p>';

    var myLatLng = {
        lat: 51.337122,
        lng: 22.378178
    };

    var center = {
        lat: 51.34,
        lng: 22.355
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: center,
        scrollwheel: false
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: 'images/marker.png'
    });

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
    google.maps.event.addDomListener(window, "resize", function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });
}