function createCookie(name, value, s) {
    if (s) {
        var date = new Date();
        date.setMonth(date.getMonth() + s)
        var expires = "; expires=" + date.toGMTString();
    } else
        var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    } else {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
            end = dc.length;
        }
    }
    return unescape(dc.substring(begin + prefix.length, end));
}

$(document).ready(function () {
    function re1() {
        if ($('.accordion').length > 0) {
            if (Modernizr.mq('screen and (max-width:1199px)')) {
                //its text page
                if ($('.r1').is(':empty')) {
                    var zobac = $('.r2').html();
                    $('.r2').html('');
                    $('.r1').html(zobac);
                }
            } else {
                if ($('.r2').is(':empty')) {
                    var zobac2 = $('.r1').html();
                    $('.r1').html('');
                    $('.r2').html(zobac2);
                }
            }
        }
    }

    re1();

    $('.kontakt-item').matchHeight();
    $('.aktualnosci-item').matchHeight();

    $('.navbar-toggle').click(function () {
        $(this).next().toggle();
        var el = $(this);
        if (el.hasClass('nactive')) {
            el.removeClass('nactive');
        } else {
            el.addClass('nactive');
        }
    });

    $(window).resize(function () {
        re1();
    });

    $(".drop span").on("click", function () {
        var el = $(this);
        if (el.hasClass('active')) {
            el.removeClass('active');
            el.parent().find('ul').stop().slideUp();
        } else {
            $('.drop ul').stop().slideUp();
            $('.drop .active').removeClass('active');
            el.addClass('active');
            el.parent().find('ul').stop().slideDown();
        }
    });


    $('#cookies-info .close').on('click', function () {
        $('#cookies-info').animate({
            'height': '0',
            'min-height': 0
        }, 300, function () {
            $(this).hide()
        });
        createCookie("cookies_info", 1, 12);
    });

    var myCookie = getCookie("cookies_info");

    if (myCookie == null) {
        $('#cookies-info').show();
    }


    function inViw() {
        $('.aktualnosci').children('.aktualnosci-item').each(function (index, el) {
            if (!$(el).hasClass('inviewShow')) {
                $(el).bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
                    if (isInView) {
                        if (visiblePartY == 'top') {
                            $(this).addClass('inviewShow inviewAnim');
                        } else {
                            $(this).addClass('inviewShow');
                        }
                    }
                });
            }
        })
    }

    if ($('.aktualnosci-item').length) {
        inViw();
    }

    function lastAddedLiveFunc() {
        if ($('#lastPostsLoader').length) {
            if ($('#lastPostsLoader').html() == '') {
                $('#lastPostsLoader').html('<div class="loading"></div>');
                $.ajax({
                    type: 'POST',
                    url: "getPosts.php",
                    data: {
                        'offset': $('#lastPostsLoader').attr('data-offset')
                    },
                    dataType: 'json',
                    success: function (data) {
                        var divPostLoader = $('#lastPostsLoader');
                        divPostLoader.before(data.list);
                        divPostLoader.remove();
                        inViw();
                    },
                    error: function () {
                        $('#lastPostsLoader').empty();
                    }
                });
            }
        }
    };

    //lastAddedLiveFunc();
    $(window).scroll(function () {

        var wintop = $(window).scrollTop(),
            docheight = $(document).height(),
            winheight = $(window).height();
        var scrolltrigger = 0.95;

        if ((wintop / (docheight - winheight)) > scrolltrigger) {
            //console.log('scroll bottom');
            lastAddedLiveFunc();
        }
    });

    $(window).load(function () {

        $('.owl-carousel').owlCarousel({
            items: 1,
            margin: 0,
            nav: true,
            autoHeight: true,
            responsiveClass: true,
            loop: true,
            onInitialized: function () {
                $('.outer-slider .loading').hide();
            }
        });
    });

});