function initMap() {

    var contentString = '<b style="font-size:14px;">P.P.H.U. Iwelto Premium Tomasz Grzywacz</b><br><br><p class="mb0">Bogucin 116, 21-080 Garbów</p><p class="mb0">tel: <a href="tel:695-165-940">695-165-940</a> Tomasz Grzywacz</p><p class="mb0">tel: <a href="tel:691-695-282">691-695-282</a> Izabela Grzywacz</p><p class="mb0">e-mail: <a href="mailto:tomasz.iwelto@interia.pl">tomasz.iwelto@interia.pl</a></p>';

    var myLatLng = {
        lat: 51.337122,
        lng: 22.378178
    };

    var center = {
        lat: 51.34,
        lng: 22.355
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: center,
        scrollwheel: false
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: 'images/marker.png'
    });

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
    google.maps.event.addDomListener(window, "resize", function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });
}
function createCookie(name, value, s) {
    if (s) {
        var date = new Date();
        date.setMonth(date.getMonth() + s)
        var expires = "; expires=" + date.toGMTString();
    } else
        var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    } else {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
            end = dc.length;
        }
    }
    return unescape(dc.substring(begin + prefix.length, end));
}

$(document).ready(function () {
    function re1() {
        if ($('.accordion').length > 0) {
            if (Modernizr.mq('screen and (max-width:1199px)')) {
                //its text page
                if ($('.r1').is(':empty')) {
                    var zobac = $('.r2').html();
                    $('.r2').html('');
                    $('.r1').html(zobac);
                }
            } else {
                if ($('.r2').is(':empty')) {
                    var zobac2 = $('.r1').html();
                    $('.r1').html('');
                    $('.r2').html(zobac2);
                }
            }
        }
    }

    re1();

    $('.kontakt-item').matchHeight();
    $('.aktualnosci-item').matchHeight();

    $('.navbar-toggle').click(function () {
        $(this).next().toggle();
        var el = $(this);
        if (el.hasClass('nactive')) {
            el.removeClass('nactive');
        } else {
            el.addClass('nactive');
        }
    });

    $(window).resize(function () {
        re1();
    });

    $(".drop span").on("click", function () {
        var el = $(this);
        if (el.hasClass('active')) {
            el.removeClass('active');
            el.parent().find('ul').stop().slideUp();
        } else {
            $('.drop ul').stop().slideUp();
            $('.drop .active').removeClass('active');
            el.addClass('active');
            el.parent().find('ul').stop().slideDown();
        }
    });


    $('#cookies-info .close').on('click', function () {
        $('#cookies-info').animate({
            'height': '0',
            'min-height': 0
        }, 300, function () {
            $(this).hide()
        });
        createCookie("cookies_info", 1, 12);
    });

    var myCookie = getCookie("cookies_info");

    if (myCookie == null) {
        $('#cookies-info').show();
    }


    function inViw() {
        $('.aktualnosci').children('.aktualnosci-item').each(function (index, el) {
            if (!$(el).hasClass('inviewShow')) {
                $(el).bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
                    if (isInView) {
                        if (visiblePartY == 'top') {
                            $(this).addClass('inviewShow inviewAnim');
                        } else {
                            $(this).addClass('inviewShow');
                        }
                    }
                });
            }
        })
    }

    if ($('.aktualnosci-item').length) {
        inViw();
    }

    function lastAddedLiveFunc() {
        if ($('#lastPostsLoader').length) {
            if ($('#lastPostsLoader').html() == '') {
                $('#lastPostsLoader').html('<div class="loading"></div>');
                $.ajax({
                    type: 'POST',
                    url: "getPosts.php",
                    data: {
                        'offset': $('#lastPostsLoader').attr('data-offset')
                    },
                    dataType: 'json',
                    success: function (data) {
                        var divPostLoader = $('#lastPostsLoader');
                        divPostLoader.before(data.list);
                        divPostLoader.remove();
                        inViw();
                    },
                    error: function () {
                        $('#lastPostsLoader').empty();
                    }
                });
            }
        }
    };

    //lastAddedLiveFunc();
    $(window).scroll(function () {

        var wintop = $(window).scrollTop(),
            docheight = $(document).height(),
            winheight = $(window).height();
        var scrolltrigger = 0.95;

        if ((wintop / (docheight - winheight)) > scrolltrigger) {
            //console.log('scroll bottom');
            lastAddedLiveFunc();
        }
    });

    $(window).load(function () {

        $('.owl-carousel').owlCarousel({
            items: 1,
            margin: 0,
            nav: true,
            autoHeight: true,
            responsiveClass: true,
            loop: true,
            onInitialized: function () {
                $('.outer-slider .loading').hide();
            }
        });
    });

});